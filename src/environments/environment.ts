// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDO_MathOgyTCaaboHE2NaE1d0QLt0wi4E",
    authDomain: "teste-dd97d.firebaseapp.com",
    projectId: "teste-dd97d",
    storageBucket: "teste-dd97d.appspot.com",
    messagingSenderId: "1010015135920",
    appId: "1:1010015135920:web:311dcde1ab7f4e9533b375",
    measurementId: "G-9ED6XRQ6V7"
  },
  custom_api: "https://us-central1-teste-dd97d.cloudfunctions.net/customapi"
};
