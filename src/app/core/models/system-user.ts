import { Model } from "./model";

export class SystemUser extends Model {
    uid?: string;
    email?: string;
    name?: string;
    birthDate?: string;
    state?: string;
    city?: string;
    gender?: string;
}