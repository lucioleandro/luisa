import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ServiceFirebase } from '../services/servicefirebase.service';
import { ServerLog } from './server-log';

@Injectable({ providedIn: 'root' })
export class ServerLogService extends ServiceFirebase<ServerLog> {

  constructor(firestore: AngularFirestore) {
    super(ServerLog, firestore, 'logs');
  }

}
