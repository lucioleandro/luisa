import { Model } from "../models/model";

export class ServerLog extends Model {
  uid: string
  message: string;
  url: string;
  userName: string;
  stack: string;
}
