import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

const KEY = 'luisatoken';

@Injectable({ providedIn: 'root' })
export class TokenService {
    
    private authJwtToken: string
    
    constructor(private afAuth: AngularFireAuth) {
        afAuth.idToken.subscribe(jwt => this.authJwtToken = jwt);
    }
    
    hasToken() {
        return !!this.authJwtToken;
    }

    getToken() {
        return this.authJwtToken;
    }

}
