import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUser$: Observable<firebase.User>;
  private idTokenResult$: Observable<firebase.auth.IdTokenResult>;

  constructor(private afAuth: AngularFireAuth) {
    this.currentUser$ = afAuth.authState;
    this.idTokenResult$ = this.afAuth.idTokenResult;
  }

  resetPassword(email: string) {
    return this.afAuth.sendPasswordResetEmail(email);
  }

  authUser() {
    return this.currentUser$;
  }

  idTokenResult() {
    return this.idTokenResult$;
  }

  loginEmailAndPassword(email: string, senha: string) {
    return this.afAuth.signInWithEmailAndPassword(email, senha);
  }

  loginGoogle() {
    return this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()
            .setCustomParameters({prompt: 'select_account'}));
  }

  logout(): Promise<void> {
    return this.afAuth.signOut();
  }

  singUp(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password);
  }

  deleteUser() {
    return this.afAuth.currentUser.then(user => user.delete());
  }

}
