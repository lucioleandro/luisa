import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { plainToClass } from 'class-transformer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Model } from '../models/model';
import { ICrud } from './icrud.interface';

export abstract class ServiceFirebase<T extends Model> implements ICrud<T> {

    ref: AngularFirestoreCollection<T>;

    constructor(protected type: { new(): T;},
                protected fireStore: AngularFirestore,
                public path: string) {

        this.ref = this.fireStore.collection<T>(this.path);
    }

    create(item: T): Promise<void> {
        return this.ref.add(item).then(res => {
            item.uid = res.id;
            this.ref.doc(res.id).set(item);
        });
    }

    update(item: T): Promise<void> {
        return this.ref.doc(item.uid).set(item);
    }

    get(id: string): Observable<T> {
        let doc = this.ref.doc<T>(id);
        return doc.get().pipe(map(snapshot => this.docToClass(snapshot)));
    }

    list(): Observable<T[]> {
        return this.ref.valueChanges();
    }

    delete(id: string): Promise<void> {
        return this.ref.doc(id).delete();
    }

    protected docToClass(snapshotDoc): T {
        let obj = {
            id: snapshotDoc.id,
            ...(snapshotDoc.data() as T)
        };
        let typed = plainToClass(this.type, obj);
        return typed;
    }

}