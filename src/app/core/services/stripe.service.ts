import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { first, map } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class StripeService {

  constructor(private authService: AuthenticationService,
              private fireStore: AngularFirestore,
              private fireFunctions: AngularFireFunctions) { }

  async sendToCheckout(typeSubscription: string, promotionCode?: string) {
    await this.authService.authUser()
      .pipe(
        map((user) => {
          return this.fireStore
            .collection('customers').doc(user.uid)
            .collection('checkout_sessions')
            .add({
              price: typeSubscription == 'mensal' ? 'price_1Jn5fLGM0nu2Sf1aZRr2B5lp' : 'price_1JnXI3GM0nu2Sf1akwNL48DX',
              allow_promotion_codes: true,
              success_url: window.location.origin + '/luisa#/checkout-premium-success',
              cancel_url: window.location.origin + '/luisa#/app/home'
            })
            .then((docRef) => {
              docRef.onSnapshot(async (snap) => {
                const { error, url } = snap.data();
                if (error) {
                  alert(`Houve um erro: ${error.message}`);
                }
                if (url) {
                  window.location.assign(url);
                }
              });
            }),
            first() // Tratar memory leak
        }),
      )
      .toPromise();
  }

  async sendToCustomerPortal() {
    const functionRef = this.fireFunctions
      .httpsCallable('ext-firestore-stripe-subscriptions-createPortalLink');
      await functionRef({ returnUrl: window.location.origin }).subscribe();
    // window.location.assign(data.url);
  }
}
