import { Observable } from 'rxjs';

export interface ICrud<T> {
    create(item: T): Promise<void>
    
    update(item: T): Promise<void>

    get(id: string): Observable<T>;

    list(): Observable<T[]>;

    delete(id: string): Promise<void>;
}