import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private service: MessageService) { }

  showSuccessMessage(message: string, detail: string) {
    this.service.add({severity: 'success', summary: message, detail: detail, life: 5000});
  }

  showErrorMessage(message: string, detail: string) {
    this.service.add({severity:'error', summary: message, detail: detail, life: 5000});
  }

}
