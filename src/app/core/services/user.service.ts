import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';
import { SystemUser } from '../models/system-user';
import { ServiceFirebase } from './servicefirebase.service';

const customApi = environment.custom_api;

@Injectable({ providedIn: 'root' })
export class UserService extends ServiceFirebase<SystemUser> {

    constructor(firestore: AngularFirestore, private http: HttpClient) {
        super(SystemUser, firestore, 'users');
    }

    registerProfile(profile: SystemUser) {
        return this.http.post(customApi, profile);
    }

}