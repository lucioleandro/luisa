import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterNewProfileComponent } from './register-new-profile.component';


const routes: Routes = [
  { 
    path: '', component: RegisterNewProfileComponent,
    data: {
      title: 'Registrar Usuário',
    } 
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterNewProfileRoutingModule { }
