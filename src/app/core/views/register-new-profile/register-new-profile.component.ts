import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { LoadingService } from 'src/app/share/components/loading/loading.service';
import { SystemUser } from '../../models/system-user';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationService } from '../../services/notification.service';
import { UserService } from '../../services/user.service';


@Component({
  templateUrl: './register-new-profile.component.html',
  styleUrls: ['./register-new-profile.component.scss']
})
export class RegisterNewProfileComponent implements OnInit {

  newUserForm: FormGroup;

  loadingBlock: boolean;

  user: SystemUser;

  constructor(private formBuilder: FormBuilder, 
              private authService: AuthenticationService,
              private loadingService: LoadingService,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.user = {};
    this.loadingBlock = false;
    this.buildForm();
    this.authService.authUser().pipe(take(1)).subscribe(us => {
      this.user.uid = us.uid;
      this.user.email = us.email;
      this.user.name = us.displayName;
      if(this.user.name) {
        this.newUserForm.get('name').disable();
      }
    });
  }

  register() {
    this.loadingService.start();
    this.loadingBlock = true;
    this.userService.registerProfile(this.user)
      .subscribe(
        () => {
          this.authService.authUser().pipe(take(1))
          .subscribe(currentUser => {
            currentUser.updateProfile({displayName: this.user.name})
            .then(() => {
              currentUser.getIdToken(true).then(() => {
                this.router.navigate(['app/home']);
                this.loadingService.stop();
                this.loadingBlock = false;
              });
            })
          });
        },
        err => {
          this.loadingService.stop();
          this.loadingBlock = false;
          this.notificationService.showErrorMessage('Houve um erro', 'Tente novamente em alguns instantes');
        });
  }

  cancel() {
    this.authService.deleteUser().then(() => {
      this.router.navigate(['login']);
    }).catch(error => {
      if(error.code === 'auth/requires-recent-login') {
        this.authService.logout().then(() => this.router.navigate(['']));
      }
    })
  }

  buildForm() {
    this.newUserForm = this.formBuilder.group({
      name: [{value: ''}, [Validators.required]],
      email: [{value: '', disabled: true}, [Validators.required]],
      birthdate: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      gender: ['Não Informado',]
    });
  }

}
