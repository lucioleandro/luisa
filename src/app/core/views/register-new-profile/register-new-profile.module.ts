import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlockUIModule } from 'primeng/blockui';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';
import { RegisterNewProfileRoutingModule } from './register-new-profile-routing.module';
import { RegisterNewProfileComponent } from './register-new-profile.component';


@NgModule({
  declarations: [RegisterNewProfileComponent],
  imports: [
    CommonModule,
    RegisterNewProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ToastModule,
    BlockUIModule,
    ProgressSpinnerModule
    
  ]
})
export class RegisterNewProfileModule { }
