import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import firebase from 'firebase/compat/app';
import { LoadingService } from 'src/app/share/components/loading/loading.service';
import { PlatformDetectorService } from '../../platform-detector/platform-detector';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationService } from '../../services/notification.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  registerForm: FormGroup;
  rescuePasswordForm: FormGroup;
  fromUrl: string;

  isNewRegister: boolean;
  isRescueModalVisible: boolean;

  @ViewChild('emailInput')
  userNameInput: ElementRef<HTMLInputElement>
  
  constructor(private authService: AuthenticationService,
              private af: AngularFireAuth,
              private loadingService: LoadingService,
              private router: Router, private formBuilder: FormBuilder,
              private activatedRout: ActivatedRoute,
              private notificationService: NotificationService,
              private platformDetectorService: PlatformDetectorService) {}

  ngOnInit(): void {
    this.isNewRegister = false;
    this.buildForms();
    this.activatedRout
      .queryParams
      .subscribe(params => 
        this.fromUrl = params['fromUrl']);

    this.activatedRout.url.subscribe(url => {
      url[0].path === 'login' ? this.isNewRegister = false : this.isNewRegister = true;
    });
    
  }

  createUSerWithEmailAndPassword() {
    const email = this.registerForm.get('email').value;
    const password = this.registerForm.get('senha').value;

    this.loadingService.start();
    this.authService.singUp(email, password).then(result => {
      result.user.sendEmailVerification();
      this.flowSuccessLogin(result);
      this.loadingService.stop();
    }).catch(error => {
      this.notificationService.showErrorMessage("Ocorreu um erro", error);
      this.loadingService.stop();
    });
  }

  loginGoogle() {
    this.authService.loginGoogle()
    .then(result => {
      this.flowSuccessLogin(result);
    }).catch(error => {
      if(!error.message.startsWith('Firebase: The popup has been closed')) {
        this.notificationService.showErrorMessage('dados de login inválidos', error);
      }
    });
  }

  loginEmailAndPassword() {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('senha').value;

    this.loadingService.start();
    this.authService.loginEmailAndPassword(email, password)
    .then(result => {
      this.flowSuccessLogin(result);
      this.loadingService.stop();
    }).catch(error => {
      this.notificationService.showErrorMessage('dados de login inválidos',"");
      this.platformDetectorService.isPlatformBrowser() && this.userNameInput.nativeElement.focus();
      this.loadingService.stop();
    });
  }

  private flowSuccessLogin(result: firebase.auth.UserCredential) {
    result.user.getIdTokenResult(true).then(data => {
      if(!data.claims?.isProfileCompleted) {
        this.router.navigate(['register-new-profile']);
        return;
      }
      this.fromUrl
      ? this.router.navigateByUrl(this.fromUrl)
      : this.router.navigate(['app/home']);
    });
  }

  rescuePassword() {
    const email = this.rescuePasswordForm.get('email').value;
    this.af.sendPasswordResetEmail(email)
      .then(() => {
        this.notificationService.showSuccessMessage('Email de recuperação enviado', 'Acesse o seu email para recuperar a senha');
        this.isRescueModalVisible = false;
        this.rescuePasswordForm.get('email').reset();
      }).catch(error => {
        this.notificationService.showErrorMessage('Email de recuperação não enviado', error);
      });
    }

  buildForms() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      senha: ['', Validators.required]
    });

    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      senha: ['', Validators.required]
    });

    this.rescuePasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
    });
  }

}
