import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: './game-wrapper.component.html',
    styleUrls: ['./game-wrapper.component.scss']
})
export class GameWrapperComponent implements OnInit {

    gameName: string;

    constructor(private activatedRoute: ActivatedRoute){}

    ngOnInit() {
        this.gameName = this.activatedRoute.firstChild.firstChild.snapshot.data['game'].name;
    }

}