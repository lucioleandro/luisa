import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({providedIn: 'root'})
export class CheckoutPremiumSuccessGuard implements CanActivate {

    constructor(private authService: AuthenticationService, private router: Router) { }

    canActivate() {
        return this.authService.authUser().pipe(map(user => {
            if(user) {
                user.getIdToken(true)
                    .then(() => {
                        this.router.navigate(['app/home']);
                        return false;
                    })
            }
            return false;
        }));
    }
}