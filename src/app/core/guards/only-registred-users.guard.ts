import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({providedIn: 'root'})
export class OnlyRegistredUsersGuard implements CanActivate {
    constructor(private authService: AuthenticationService, 
                private router: Router) { }

    canActivate(): Observable<boolean> {
        return this.authService.idTokenResult().pipe(map(tokenResult => {
            if(tokenResult.claims && tokenResult.claims?.isProfileCompleted) {
                return true;
            }
            this.router.navigate(['register-new-profile']);
            return false;
        }),
        catchError((err) => {
            this.router.navigate(['register-new-profile']);
            return of(false);
        }));
    }
}