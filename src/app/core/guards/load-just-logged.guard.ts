import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({providedIn: 'root'})
export class LoadJustLoggedGuard implements CanLoad {
    constructor(private authService: AuthenticationService) { }

    canLoad(): Observable<boolean> {
        return this.authService.authUser().pipe(map(user => {
            if(user) {
                return true;
            }
            return false;
        }));
    }
}