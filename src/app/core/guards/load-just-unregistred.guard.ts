import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { TokenService } from '../services/token.service';

@Injectable({providedIn: 'root'})
export class LoadJustUnregistredGuard implements CanLoad {
    constructor(private tokenService: TokenService, private authService: AuthenticationService) { }

    canLoad(): Observable<boolean> {
        return this.authService.idTokenResult().pipe(map(token => {
            if(token && !token.claims.isProfileCompleted) {
                return true;
            }
            return false;
        }))
    }
}