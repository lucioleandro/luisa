import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({providedIn: 'root'})
export class OnlyNotRegistredUserGuard implements CanActivate {

    constructor(private authService: AuthenticationService, private router: Router) { }

    canActivate(): Observable<boolean> {
        return this.authService.idTokenResult().pipe(map(tokenResult => {
            if(!tokenResult.claims?.isProfileCompleted) {
                return true;
            }
            this.router.navigate(['/app/home/']);
            return false;
        }),
        catchError((err) => {
            this.router.navigate(['/app/home/']);
            return of(false);
        }));
    }
}