import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({providedIn: 'root'})
export class OnlyProfileOwnerGuard implements CanActivate {

    constructor(private authService: AuthenticationService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot) {

        return this.authService.authUser().pipe(map(user => {
            if(user.uid !== route.params.profileId) {
                this.router.navigate(['app/profile/', user.uid]);
                return false;
            }
            return true;
        }));
    }
}