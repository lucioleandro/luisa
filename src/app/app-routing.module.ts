import { NgModule } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/compat/auth-guard';
import { RouterModule } from '@angular/router';
import { AppMainComponent } from './app.main.component';
import { GameWrapperComponent } from './core/game/components/game-wrapper/game-wrapper.component';
import { CheckoutPremiumSuccessGuard } from './core/guards/chechout-premium-success.guard';
import { LoadJustLoggedGuard } from './core/guards/load-just-logged.guard';
import { LoadJustUnregistredGuard } from './core/guards/load-just-unregistred.guard';
import { OnlyNotRegistredUserGuard } from './core/guards/only-not-registred-user.guard';
import { OnlyProfileOwnerGuard } from './core/guards/only-profile-owner.guard';
import { OnlyRegistredUsersGuard } from './core/guards/only-registred-users.guard';
import { LandingPageComponent } from './core/views/landing-page/landing-page.component';
import { LoginComponent } from './core/views/login/login.component';
import { HomeComponent } from './site/views/home/home.component';


const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectAlreadyLoggedToHome = () => redirectLoggedInTo(['app/home']);

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', 
                component: LandingPageComponent, 
                data: {
                    title: 'Luisa',
                }
            },
            {
                path: 'app', 
                component: AppMainComponent,
                children: [
                    {path: '', pathMatch: 'full', redirectTo: '/app/home'},
                    {
                        path: 'home', 
                        component: HomeComponent,
                        canActivate: [AngularFireAuthGuard, OnlyRegistredUsersGuard],
                        data: { 
                            title: 'Home', 
                            authGuardPipe: redirectUnauthorizedToLogin
                        }
                    },
                    {
                        path: 'profile/:profileId',
                        canActivate: [AngularFireAuth, OnlyRegistredUsersGuard, OnlyProfileOwnerGuard],
                        data: {
                            title: 'Profile',
                            authGuardPipe: redirectUnauthorizedToLogin
                        },
                        loadChildren: () => import('./site/views/user-profile/user-profile.module').then(m => m.UserProfileModule)
                    },
                    {
                        path: 'game',
                        component: GameWrapperComponent,
                        canActivate: [AngularFireAuthGuard, OnlyRegistredUsersGuard],
                        children: [
                            {
                                path: '',
                                redirectTo: '/home',
                                pathMatch: 'full'
                              },
                            {
                                path: 'conjugate-verb-challenge', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugue Challeng',
                                    game: {
                                        name: 'Conjugue Challeng - modo speed',
                                        description: 'Treine suas habilidades de conjugação com este desafio',
                                        category: 'Conjugação de Verbo',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                            {
                                path: 'conjugate-verb-challengeII', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugue Challeng ',
                                    game: {
                                        name: 'Conjugue Challenge II - modo precisão',
                                        description: 'Treine suas habilidades de conjugação com este desafio',
                                        category: 'Conjugação de Verbo',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                            {
                                path: 'conjugate-verb-challengeII', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugue Challeng ',
                                    game: {
                                        name: 'Conjugue Challenge II - modo precisão',
                                        description: 'Treine suas habilidades de conjugação com este desafio',
                                        category: 'Conjugação de Verbo',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                            {
                                path: 'conjugate-verb-challengeII', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugue Challeng ',
                                    game: {
                                        name: 'Conjugue Challenge II - modo precisão',
                                        description: 'Treine suas habilidades de conjugação com este desafio',
                                        category: 'Conjugação de Verbo',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                            {
                                path: 'passive-voice', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugate Verb',
                                    game: {
                                        name: 'Conjugate Verb Challenge',
                                        description: 'Treine suas habilidades de voz passiva com este desafio',
                                        category: 'Voz Passiva',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                            {
                                path: 'passive-voice2', 
                                canLoad: [LoadJustLoggedGuard],
                                data: {
                                    title: 'Conjugate Verb',
                                    game: {
                                        name: 'Conjugate Verb Challenge',
                                        description: 'Treine suas habilidades de voz passiva com este desafio',
                                        category: 'Voz Passiva',
                                    }
                                },
                                loadChildren: () => import('./site/views/games/conjugate-verb-challenge/conjugate-verb-challenge.module')
                                .then(m => m.ConjugateVerbChallengeModule)
                            },
                        ]
                    },
                ]
            },
            {
                path: 'login', 
                component: LoginComponent, 
                canActivate: [AngularFireAuthGuard],
                data: {
                    title: 'login',
                    authGuardPipe: redirectAlreadyLoggedToHome
                }
            },
            {
                path: 'register', 
                component: LoginComponent, 
                canActivate: [AngularFireAuthGuard],
                data: {
                    title: 'login',
                    authGuardPipe: redirectAlreadyLoggedToHome
                }
            },
            { 
                path: 'register-new-profile',
                canLoad: [LoadJustLoggedGuard, LoadJustUnregistredGuard], 
                loadChildren: () => import('./core/views/register-new-profile/register-new-profile.module').then(m => m.RegisterNewProfileModule),
                canActivate: [AngularFireAuthGuard, OnlyNotRegistredUserGuard],
                data: {
                    authGuardPipe: redirectUnauthorizedToLogin
                }
            },
            {
                path: 'terms-of-use',
                loadChildren: () => import('./site/views/terms-of-use/terms-of-use.module').then(m => m.TermsOfUseModule)
            },
            {
                path: 'privacy-policy',
                loadChildren: () => import('./site/views/privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)
            },
            {
                path: 'checkout-premium-success',
                canActivate: [CheckoutPremiumSuccessGuard],
                loadChildren: () => import('./core/views/not-found-page/not-found-page.module').then(m => m.NotFoundPageModule) 
            },
            { 
                path: 'not-found-page',
                canLoad: [LoadJustLoggedGuard],
                loadChildren: () => import('./core/views/not-found-page/not-found-page.module').then(m => m.NotFoundPageModule) 
            },
            
            {path: '**', redirectTo: '/not-found-page'},
        ], { scrollPositionRestoration: 'enabled', relativeLinkResolution: 'legacy' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
