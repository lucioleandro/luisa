import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAnalyticsModule, ScreenTrackingService, UserTrackingService } from '@angular/fire/compat/analytics';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Application services
import { MessageService } from 'primeng/api';
import { BlockUIModule } from 'primeng/blockui';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RippleModule } from 'primeng/ripple';
import { TagModule } from 'primeng/tag';
import { ToastModule } from 'primeng/toast';
import { environment } from 'src/environments/environment';
// Application Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppMainComponent } from './app.main.component';
import { GameWrapperComponent } from './core/game/components/game-wrapper/game-wrapper.component';
import { GlobalErrorHandler } from './core/global-error-handler/global-error-handler';
import { RequestInterceptor } from './core/interceptors/auth.request.interceptor';
import { LandingPageComponent } from './core/views/landing-page/landing-page.component';
import { LoginComponent } from './core/views/login/login.component';
import { LoadingModule } from './share/components/loading/loading.module';
import { FormMessagesModule } from './share/components/messages/messages.module';
import { EmailConfirmWarningComponent } from './site/views/home/email-confirm-warning/email-confirm-warning.component';
import { GameListComponent } from './site/views/home/game-list/game-list.component';
import { HomeComponent } from './site/views/home/home.component';
import { SelectPremimPlanComponent } from './site/views/home/select-premim-plan/select-premim-plan.component';

@NgModule({
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAnalyticsModule,
        FormsModule,
        ReactiveFormsModule,
        AngularFirestoreModule,
        AngularFireAuthModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RippleModule,
        ProgressSpinnerModule,
        ButtonModule,
        LoadingModule,
        FormMessagesModule,
        ToastModule,
        DialogModule,
        BlockUIModule,
        TagModule
    ],
    declarations: [
        AppComponent,
        AppMainComponent,
        HomeComponent,
        LoginComponent,
        GameListComponent,
        GameWrapperComponent,
        LandingPageComponent,
        EmailConfirmWarningComponent,
        SelectPremimPlanComponent,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestInterceptor,
            multi: true
          },
         MessageService,
         ScreenTrackingService,
         UserTrackingService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
