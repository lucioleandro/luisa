import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Verb } from '../domain/verb';
import { TenseEnum } from '../views/games/conjugate-verb-challenge/domain/tenseEnum';



@Injectable({providedIn: 'root'})
export class VerbService {

    apiUrl = '/assets/data/verbs.json';
    verbs: Verb[] = [];

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get(this.apiUrl).pipe(map((response: any) => {
            this.verbs = response.verbs;
            return this.verbs;
        }));
    }

    getTenses() {
        return [TenseEnum.simplepresent, TenseEnum.simplepast, TenseEnum.simplefuture];
    }

    getModes() {
        return ['afirm', 'negative', 'inter'];
    }

    getPronouns() {
        return ['I', 'you', 'he', 'she', 'it', 'we', 'they'];
    }

}