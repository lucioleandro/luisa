export interface Game {
    name?: string;
    category?: string;
    description?: string;
    route?: string; 
}