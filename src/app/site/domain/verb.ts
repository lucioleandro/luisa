export interface Verb {
    infinitive?: string;
    simplepresent?: string;
    simplepast?: string;
    pastparticiple?: string;
    portugueseMeanings?: string[]
    dictionaryDefinition?: string;
}