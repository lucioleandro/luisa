import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/core/services/authentication.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private authService: AuthenticationService) { }

  user$: Observable<firebase.User>;

  ngOnInit(): void {
    this.user$ = this.authService.authUser();
  }

}
