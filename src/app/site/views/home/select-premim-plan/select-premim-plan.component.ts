import { Component, Input, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import { Dialog } from 'primeng/dialog';
import { take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { StripeService } from 'src/app/core/services/stripe.service';

@Component({
  selector: 'app-select-premim-plan',
  templateUrl: './select-premim-plan.component.html',
  styleUrls: ['./select-premim-plan.component.scss']
})
export class SelectPremimPlanComponent implements OnInit {

  @Input() dialog: Dialog
  user: firebase.User;
  loadingBlock: boolean;

  constructor(private stripeService: StripeService,
              private authService: AuthenticationService, 
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.authService.authUser().pipe(take(1))
      .subscribe(user => this.user = user);
  }

  subscribe(typeSubscription: string) {
    if(this.user.emailVerified) {
      this.loadingBlock = true;
      this.stripeService.sendToCheckout(typeSubscription)
      .then(() => this.loadingBlock = false)
      .catch(() => this.loadingBlock = false);
      return;
    }
    this.notificationService.showErrorMessage('Para ser premium é necessário que seu email seja verificado', 'Verifique o seu email');
    this.dialog.close(new Event(''));
  }

}
