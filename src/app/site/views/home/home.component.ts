import { Component, OnInit, ViewChild } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Dialog } from 'primeng/dialog';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Game } from '../../domain/game';


@Component({
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

    @ViewChild('dialog', { static: false }) dialogComponent: Dialog;

    listOfGamesMap: Map<string, Game[]>;

    memberModalVisible: boolean;

    constructor(private router: Router, 
                public authService: AuthenticationService) { }

    ngOnInit() {
        this.memberModalVisible = false;
        this.listOfGamesMap = new Map<string, Game[]>()
        this.initListOfGames();
    }

    private initListOfGames() {
        let listOfGames: Game[] = [];
        let routes = this.router.config[1].children[3].children;
        for (let index = 0; index < routes.length; index++) {
            if(this.isAGameRoute(routes[index])) {
                let game: Game = {};
                game.name = routes[index].data.game.name;
                game.category = routes[index].data.game.category;
                game.description = routes[index].data.game.description;
                game.route = routes[index].path;
                listOfGames.push(game);
            }
        }
        listOfGames.forEach(game => {
            if(!this.listOfGamesMap.has(game.category)) {
                let gameArray: Game[] = [];
                gameArray.push(game);
                this.listOfGamesMap.set(game.category, gameArray);

            } else {
                let gameArray = this.listOfGamesMap.get(game.category);
                gameArray.push(game);
                this.listOfGamesMap.set(game.category, gameArray);
            }
        });
    }

    private isAGameRoute(route: Route) {
        return route.data?.hasOwnProperty('game');
    }

    openPremiumPlanModal() {
        this.memberModalVisible = true;
        this.dialogComponent.maximize();
    }

}
