import { Component, Input, OnInit } from '@angular/core';
import { Game } from 'src/app/site/domain/game';

@Component({
  selector: 'app-game-list[gamesMap]',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  @Input() gamesMap: Map<string, Game[]>;

  constructor() { }

  ngOnInit() {
  }

  trackByFn(_, game: Game) {
    return game.name;
  }
  
}
