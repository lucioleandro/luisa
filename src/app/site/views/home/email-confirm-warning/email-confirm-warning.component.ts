import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat/app';
import { take } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-email-confirm-warning',
  templateUrl: './email-confirm-warning.component.html',
  styleUrls: ['./email-confirm-warning.component.scss']
})
export class EmailConfirmWarningComponent implements OnInit {

  displayValue: string;
  user: firebase.User;

  constructor(private authService: AuthenticationService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.authService.authUser()
    .pipe(take(1))
    .subscribe(user => {
      this.user = user;
      if(user.emailVerified) {
        this.displayValue = 'none';
      } else {
        this.displayValue = 'block'
      }
    });
  }

  resendEmailVerification() {
    this.user.sendEmailVerification().then(() => {
      this.notificationService.showSuccessMessage('Email de verificação enviado com sucesso',
        'Confira o seu email e caixa de spam');
    });
  }

  ignore() {
    this.displayValue = 'none';
    this.user.getIdToken(true);
  }

}
