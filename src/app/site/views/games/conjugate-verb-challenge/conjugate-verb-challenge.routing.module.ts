import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ConjugateVerbChallengeComponent } from "./conjugate-verb-challenge.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '', component: ConjugateVerbChallengeComponent,
                pathMatch: 'full',
                data: {title: 'Conjugação Verbo'}
            },
            {path: '**', redirectTo: '/not-found-page'},
        ])
    ],
    exports: [RouterModule]
})
export class ConjugateVerbChallengeRoutingModule {

}