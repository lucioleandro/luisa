import { Component, Input, OnInit } from "@angular/core";
import { RegisterHistoric } from "./domain/register-historic";

@Component({
    selector: 'app-historic',
    templateUrl: './historic.component.html',
    styleUrls: ['./historic.component.scss']
})
export class HistoricComponent implements OnInit {

    @Input() historico: RegisterHistoric[];

    constructor() {}

    ngOnInit(): void {
    }

}