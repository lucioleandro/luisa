import { Verb } from './../../../../../domain/verb';

export interface RegisterHistoric {
    verb?: Verb;
    combination?: string;
    yourAnswer?: string;
    correctAnswer?: string;
    statusAnswer?: string;
}