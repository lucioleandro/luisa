import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConjugateVerbChallengeComponent } from './conjugate-verb-challenge.component';
import { ScoreBoardComponent } from './scoreboard/scoreboad.component';
import { ChallengeComponent } from './challenge/challenge.component';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DialogModule } from 'primeng/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { RouterModule } from '@angular/router';
import { ConjugateVerbChallengeRoutingModule } from './conjugate-verb-challenge.routing.module';
import { HistoricComponent } from './historic/historic.component';
import {TableModule} from 'primeng/table';

@NgModule({
  imports: [
    CommonModule,
    ConjugateVerbChallengeRoutingModule,
    RouterModule,
    ButtonModule,
    TooltipModule,
    OverlayPanelModule,
    DialogModule,
    ReactiveFormsModule,
    ProgressSpinnerModule,
    InputTextModule,
    InputTextareaModule,
    TableModule
  ],
  declarations: [
    ConjugateVerbChallengeComponent,
    ScoreBoardComponent,
    ChallengeComponent,
    HistoricComponent
  ]
})
export class ConjugateVerbChallengeModule { }
