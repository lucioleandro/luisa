import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Verb } from 'src/app/site/domain/verb';
import { VerbService } from 'src/app/site/service/verb.service';
import { GameConfig } from './domain/game-config';
import { TenseEnum } from './domain/tenseEnum';
import { RegisterHistoric } from './historic/domain/register-historic';


@Component({
    templateUrl: './conjugate-verb-challenge.component.html',
    styleUrls: ['./conjugate-verb-challenge.component.scss']
})
export class ConjugateVerbChallengeComponent implements OnInit {

    gameStarted: boolean;
    crurrentRound: number;

    private verbs: Verb[];
    drawnVerb: Verb;

    private tenses: TenseEnum[];
    drawnTense: TenseEnum;

    private pronouns: string[];
    drawnPronoun: string;

    private modes: string[];
    drawnMode: string;

    timeOver: boolean;
    scoreGame: number;

    historico: RegisterHistoric[];

    displayModalConfig: boolean;
    configFormGroup: FormGroup;
    config: GameConfig;

    corretasSeguidas: number;

    constructor(private verbService: VerbService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.gameStarted = false;
        this.tenses = this.verbService.getTenses();
        this.pronouns = this.verbService.getPronouns();
        this.modes = this.verbService.getModes();
        this.displayModalConfig = false;
        this.buildForm();
        this.initGameConfigs();

        this.verbService.getAll()
        .subscribe(verbs => {
            this.verbs = verbs
        });
    }

    startGame() {
        this.putVariablesInInitialState();
        this.gameStarted = true;
        this.generateNewChallenge();
    }

    finalizeGame() {
        this.gameStarted = false;
    }

    private putVariablesInInitialState() {
        this.scoreGame = 0;
        this.crurrentRound = 0;
        this.historico = [];
        this.corretasSeguidas = 0;
    }

    processResult(result: any) {
        if(result.statusResposta === 'correta') {
            this.corretasSeguidas++;
            let bonus = this.corretasSeguidas * 3;
            let bonusPorTempo = (10 + result.tempoResposta) - 10;
            this.scoreGame += (10 + bonus + bonusPorTempo);
        } else {
            this.corretasSeguidas = 0;
            (this.scoreGame -= 5) < 0 ? this.scoreGame = 0 : this.scoreGame;
        }
        this.historico[this.crurrentRound -1].statusAnswer = result.statusResposta;
        this.historico[this.crurrentRound -1].yourAnswer = result.respostaUsuario;
        this.historico[this.crurrentRound -1].correctAnswer = result.respostaCorreta;

        this.generateNewChallenge();
    }

    private generateNewChallenge() {
        this.crurrentRound++;
        if(this.crurrentRound <= this.config.maxRounds) {
            this.clearRound();
            setTimeout(() => {
                this.drawTense();
                this.drawPronoun();
                this.drawMode();
                this.drawVerb();
                this.historico.push({verb: this.drawnVerb, 
                    combination: `Pronome: ${this.drawnPronoun} -
                      Tempo: ${this.drawnTense} -
                      Modo: ${this.drawnMode}`});
            }, 2000);
            
        }
    }

    showModalDialog() {
        this.displayModalConfig = true;
    }

    private drawVerb() {
        let randomNumber = this.getRamdomInt(this.verbs.length);
        this.drawnVerb = this.verbs[randomNumber];
        this.verbs.splice(randomNumber, 1);
    }

    private drawTense() {
        let randomNumber = this.getRamdomInt(this.tenses.length);
        this.drawnTense = this.tenses[randomNumber];
    }
    
    private drawPronoun() {
        let randomNumber = this.getRamdomInt(this.pronouns.length);
        this.drawnPronoun = this.pronouns[randomNumber];
    }

    private drawMode() {
        let randomNumber = this.getRamdomInt(this.modes.length);
        this.drawnMode = this.modes[randomNumber];
    }

    private getRamdomInt(max: number) {
        return Math.floor(Math.random() * max);
    }

    private clearRound() {
        this.drawnVerb = null;
        this.drawnTense = null;
        this.drawnPronoun = null;
        this.drawnMode = null;
        this.timeOver = false;
    }

    private buildForm() {
        this.configFormGroup = this.formBuilder.group({
            'maxRounds': ['', [Validators.required]],
            'answerTimeout': ['', [Validators.required]]
        });
    }

    private initGameConfigs() {
        this.config = {};
        this.config.answerTime = 10;
        this.config.maxRounds = 10;
    }

}