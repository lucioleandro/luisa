import { Injectable } from '@angular/core';
import { Verb } from 'src/app/site/domain/verb';
import { TenseEnum } from '../../domain/tenseEnum';

@Injectable({providedIn: 'root'})
export class ChallengeHelper {
    
    buildAnswear(verb: Verb, mode: string, tense: string, pronoun: string) {
        let answer = '';
        if(mode === 'afirm') {
            if(this.isHeSheOrIt(pronoun) && tense === 'present') {
                answer = answer + pronoun + ' ' + verb[TenseEnum[tense]] + 's';
            } else if(!this.isHeSheOrIt(pronoun) && tense === 'present') {
                answer = answer + pronoun + ' ' + verb[TenseEnum[tense]];
            } else if(tense === 'future') {
                answer = answer + pronoun + ' will ' + verb.simplepresent;
            }
            else {
                answer = answer + pronoun + ' ' + verb[TenseEnum[tense]];
            }
        } 
        else if(mode === 'inter') {
            if(tense === 'present' && this.isHeSheOrIt(pronoun)) {
                answer += 'does ';
            } else if(tense === 'present' && !this.isHeSheOrIt(pronoun)) {
                answer += 'do '
            } else if(tense === 'future') {
                answer += 'will '
            }
            else {
                answer += 'did '
            }
            answer += pronoun + ' ' + verb.simplepresent + '?';
        }
        else {
            answer += pronoun + ' ';
            if(tense === 'present' && this.isHeSheOrIt(pronoun)) {
                 answer += 'doesn\'t ' + verb.simplepresent;
            } else  if(tense === 'present' && !this.isHeSheOrIt(pronoun)) {
                answer += 'don\'t ' + verb.simplepresent;
            } else if(tense === 'future') {
                answer += 'won\'t ' + verb.simplepresent;
            }
            else {
                answer += 'didn\'t ' + verb.simplepresent;
            }
        }
        return answer;
    }

    private isHeSheOrIt(pronoun: string) {
        return pronoun === 'he' || pronoun === 'she' || pronoun === 'it';
    }
}