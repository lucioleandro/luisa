import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Verb } from 'src/app/site/domain/verb';
import { PlatformDetectorService } from './../../../../../core/platform-detector/platform-detector';
import { ChallengeHelper } from './helper/challenge.helper';

@Component({
    selector: 'app-challenge[timer]',
    templateUrl: './challenge.component.html',
    styleUrls: ['./challenge.component.css']
})
export class ChallengeComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() verb: Verb;
    @Input() tense: string;
    @Input() pronoun: string;
    @Input() mode: string;
    timeOver: boolean;

    @Output() generateNewChallenge = new EventEmitter();

    @Input() timer: number;
    interval: any;
    timeout: any;

    wrongAnswerAudio: HTMLAudioElement;
    correctAnswerAudio: HTMLAudioElement;
    tikTakAudio: HTMLAudioElement;

    @ViewChild('textArea')
    textAnswear: ElementRef;

    constructor(private helper: ChallengeHelper, 
        private platformDetectorService: PlatformDetectorService) {}

    ngOnInit() {
        this.timeOver = false;
        this.startTimer();
    }

    ngAfterViewInit() {
        this.platformDetectorService.isPlatformBrowser() && this.textAnswear.nativeElement.focus();
        this.correctAnswerAudio = new Audio('https://firebasestorage.googleapis.com/v0/b/teste-dd97d.appspot.com/o/game%2Fsounds%2Fcorrect_answer.mp3?alt=media&token=36a37dfa-fef7-493c-80bb-580c5c8a587f');
        this.wrongAnswerAudio = new Audio('https://firebasestorage.googleapis.com/v0/b/teste-dd97d.appspot.com/o/game%2Fsounds%2Fwrong_answer.mp3?alt=media&token=fda930f5-6411-4136-8538-d02256de8194');
        this.tikTakAudio = new Audio('https://firebasestorage.googleapis.com/v0/b/teste-dd97d.appspot.com/o/game%2Fsounds%2Ftiktak.mp3?alt=media&token=659b7464-eff8-4b15-af93-f38f4170f23a');
        this.correctAnswerAudio.load();
        this.wrongAnswerAudio.load();
        this.tikTakAudio.load();
        this.correctAnswerAudio.volume = 0.3;
        this.wrongAnswerAudio.volume = 0.3;
        this.tikTakAudio.volume = 0.4;
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
        clearTimeout(this.timeout);
        this.tikTakAudio.played && this.tikTakAudio.pause();
    }

    emitChallengeResult(result: any) {
        this.generateNewChallenge.emit(result);
    }

    startTimer() {
        this.interval = setInterval(() => {
            this.timer--;
            if(this.timer == 5) {
                this.tikTakAudio.play();
            }
            if(this.timer == 0) {
                this.timeOver = true;
                clearInterval(this.interval);
                this.wrongAnswerAudio.play();
                this.timeout = setTimeout(() => {
                    this.emitChallengeResult({statusResposta: 'errada', 
                    respostaUsuario: this.textAnswear.nativeElement.value, respostaCorreta: this.getAnswer(), 
                    tempoResposta: 0 });
                },1500);
            }
        }, 1000)
    }

    getMeanings() {
        return this.verb.portugueseMeanings.map((value, index) => {
            if(index > 0) {return ' ' + value;}
            return value;
        });
    }

    getAnswer() {
     return this.helper.buildAnswear(this.verb, this.mode, this.tense, this.pronoun);
    }

    sendAnswer(event: KeyboardEvent) {
        //TODO tirar espaço entre as palavras
        if(event.key === 'Enter') {
            this.tikTakAudio.pause();
            if(this.textAnswear.nativeElement.value.toLowerCase().trim() === this.getAnswer().toLowerCase().trim()) {
                this.correctAnswerAudio.play();
                this.emitChallengeResult({statusResposta: 'correta', 
                    respostaUsuario: this.textAnswear.nativeElement.value, respostaCorreta: this.getAnswer(),
                    tempoResposta: this.timer });
            } else {
                this.wrongAnswerAudio.play();
                this.emitChallengeResult({statusResposta: 'errada', 
                    respostaUsuario: this.textAnswear.nativeElement.value, respostaCorreta: this.getAnswer(),
                    tempoResposta: this.timer });
            }
        }
    }

}