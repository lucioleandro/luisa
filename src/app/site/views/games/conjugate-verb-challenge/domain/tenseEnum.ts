export enum TenseEnum {
    simplepresent = <any>'present',
    simplepast = <any>'past',
    simplefuture = <any>'future'
}   