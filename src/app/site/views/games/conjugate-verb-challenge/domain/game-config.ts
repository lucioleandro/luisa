export interface GameConfig {
    maxRounds?: number;
    answerTime?: number;
}