import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-score',
    templateUrl: './scoreboard.component.html',
    styleUrls: ['./scoreboard.component.css']
})
export class ScoreBoardComponent implements OnInit {

    @Input() score: number;

    constructor() {}

    ngOnInit() {

    }

}