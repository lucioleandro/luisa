import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PrivacyPolicyComponent } from "./privacy-policy.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: PrivacyPolicyComponent,
                pathMatch: 'full',
                data: {
                    title: 'Política de Privacidade'
                }
            },
            {path: '**', redirectTo: '/not-found-page'},
        ])
    ]
})
export class PrivacyPolicyRoutingModule { }