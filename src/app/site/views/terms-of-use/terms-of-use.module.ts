import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsOfUseComponent } from './terms-of-use.component';
import { TermsOfUseRoutingModule } from './terms-of-use.routing.module';

@NgModule({
  imports: [
    CommonModule,
    TermsOfUseRoutingModule
  ],
  declarations: [ TermsOfUseComponent ]
})
export class TermsOfUseModule { }
