import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TermsOfUseComponent } from './terms-of-use.component';

@NgModule({
  imports: [
      RouterModule.forChild([
          {
              path: '', component: TermsOfUseComponent,
              pathMatch: 'full',
              data: {
                  title: 'Termos de Uso'
              }
          },
          {path: '**', redirectTo: '/not-found-page'},
      ])
  ],
  exports: [RouterModule]
})

export class TermsOfUseRoutingModule { }
