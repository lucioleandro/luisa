import { Component } from '@angular/core';
import { Router } from '@angular/router';
import firebase from 'firebase/compat/app';
import { PrimeNGConfig } from 'primeng/api';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthenticationService } from './core/services/authentication.service';

@Component({
    selector: 'app-main',
    templateUrl: './app.main.component.html'
})
export class AppMainComponent {

    public menuActive = true;

    public topbarMenuActive = false;

    activeTopbarItem: Element;

    menuClick: boolean;

    topbarMenuButtonClick: boolean;

    menuHoverActive: boolean;

    inputStyle = 'outlined';

    ripple: boolean;

    configActive: boolean;

    configClick: boolean;

    user$: Observable<firebase.User>;

    constructor(private primengConfig: PrimeNGConfig,
        private authService: AuthenticationService,
        private router: Router) { }

    ngOnInit() {
        this.primengConfig.ripple = true;
        this.user$ = this.authService.authUser();
    }

    onTopbarMenuButtonClick(event: Event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    }

    onTopbarItemClick(event: Event, item: Element) {
        this.topbarMenuButtonClick = true;

        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        } else {
            this.activeTopbarItem = item;
        }
        event.preventDefault();
    }

    onTopbarSubItemClick(event) {
        event.preventDefault();
    }

    logout() {
        this.authService.logout().then(() => { 
            this.router.navigate(['/login'])
        }).catch(error => {
            console.log(error);
        });
    }

    goToProfile() {
        this.user$.pipe(take(1)).subscribe(user => {
            this.router.navigate(['app/profile/', user.uid]);
        });
    }
}
