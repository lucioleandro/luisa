import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormMessagesComponent } from './messages.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ FormMessagesComponent ],
  exports: [ FormMessagesComponent ]
})
export class FormMessagesModule { }
