import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-form-message',
  templateUrl: './messages.component.html',
})
export class FormMessagesComponent implements OnInit {

  @Input()
  text = '';

  constructor() {}

  ngOnInit() {
  }

}
