import {LuisaPage} from './app.po';

describe('Luisa App', () => {
    let page: LuisaPage;

    beforeEach(() => {
        page = new LuisaPage();
    });

    it('should display welcome message', () => {
        page.navigateTo();
        expect(page.getTitleText()).toEqual('Welcome to Liane!');
    });
});
