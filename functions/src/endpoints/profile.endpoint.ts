import * as functions from "firebase-functions";
import { getUserCredentialsMiddleware } from "../auth.middleware";
import { auth, db } from "../init";

const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
export const customApp = express();

customApp.use(cors);
customApp.use(cookieParser);
customApp.use(getUserCredentialsMiddleware);

customApp.post('/', async (req: any, res: any) => {
    functions.logger.log('chamando função para criação de perfil de novo usuário');
    try {
        db.doc(`users/${req.uid}`).set(req.body);
        await auth.setCustomUserClaims(req.uid, {isProfileCompleted: true});

        functions.logger.log('Profile cadastrado com sucesso')
        
        res.status(201).json({message: 'Profile cadastrado com sucesso'});
        
    } catch (error) {
        functions.logger.error('O perfil não foi cadastrado ', error);
        res.status(500).json({message: 'O perfil não foi cadastrado'})
    }
});