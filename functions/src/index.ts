import * as functions from "firebase-functions";
import { UserRecord } from "firebase-functions/v1/auth";
import { customApp } from "./endpoints/profile.endpoint";
import { auth, db } from "./init";
import { scheduleDeleteUnverifiedAccounts } from "./schedules/delete-unverified-accounts";

export const customapi = functions.https.onRequest(customApp);

export const addCustomClaim = functions.auth.user()
    .onCreate((user: UserRecord) => {
        return auth.setCustomUserClaims(user.uid, {
            isProfileCompleted: false,
        }).then(() => {
            return auth.getUser(user.uid);
        });
    });

export const deleteAccountAndSendEmail = functions.auth.user()
    .onDelete((user: UserRecord) => {
      functions.logger.log('Iniciando gatilho para excluir perfil de usuario do banco de dados');
      db.collection('users').doc(user.uid)
        .delete().then(() => {
          functions.logger.log('Perfil do usuário excluido com sucesso');
          const email = `
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html>
          <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Bém-vindo a Luisa</title>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
          
            <style>
          
            html, body{
              margin: 0;
              padding: 0;
              border: none;
            }
          </style>
          
          </head>
          <body>
          
            <table border="0" width="90%" cellpadding="0" cellspacing="0" align="center"> 
              <tr bgcolor="#000000">
                <td align="center" class="cabecalho-logo">
                  <br>
                  <br>
                </td>
              </tr>
          
              <tr>
                <td align="center" >
                  <br>
                    <h1>Luisa</h1>
                  <br>
                </td>
              </tr>
          
          
              <tr>
                <td align="center">
                  <span style="font-size: 25px; font-family: sans-serif">
                    Oi, <strong>${user.displayName}</strong>
                    <br><br>
                    Sua conta foi deletada por falta de verificação de email
                  </span>      
                </td>
              </tr>
              <!-- Parte preta -->
              <tr bgcolor="#000000">
              </tr>
            </table>
          `;
          db.collection('mail').add({
            to: user.email,
            from: '<lucio.leo85@gmail.com>',
            message: {
                subject: "Luisa | Conta Deletada",
                html: `${email}`
            }
        });
        });
    });

export const sendWelcomeEmail = functions.firestore.document('users/{documentId}')
    .onCreate((snapshot) => {
        const user = snapshot.data();

        const email = `
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html>
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Bém-vindo a Luisa</title>
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
          <style>
        
          html, body{
            margin: 0;
            padding: 0;
            border: none;
          }
        
          img {
            display: block;
          }
        
          .sem-underline {
            text-decoration: none;
          }
        
          .link-veja {
            font-family: sans-serif;
            color: #47a138;
            font-size: 13px;
          }
        
          .cta {
            background-color: #47a138;
            color: #ffffff;
            display: inline-block;
            font-family: sans-serif;
            font-size: 22px;
            line-height: 55px;
            text-align: center;
            text-decoration: none;
            width: 219px;
            -webkit-text-size-adjust: none;
          }
        
          .vantagem {
            /*border-bottom: 2px solid #195a0c;*/
            max-width: 133px;
            padding: 20px;
          }
        
          .vantagem hr {
            margin-top: 15px;
          }
        
          .vantagem-titulo {
            padding: 10px 0;
          }
        
          .vantagem-titulo {
            font-family: sans-serif;
            font-size: 15px;
          }
        
          .vantagem-texto {
            font-family: sans-serif;
            font-size: 13px;
            color: #ffffff;
          }
        
          .redeSocial {
            margin: 0 20px;
          }
        
          .rodape-texto {
            color: #ffffff;
            font-family: sans-serif;
            font-size: 13px;
            width: 70%;
            display: block;
          }
        
          .rodape-texto--link {
            color: inherit;
          }
        
          .parte-verde {
            margin-top: 20px;
          }
        
        </style>
        
        </head>
        <body>
        
          <table border="0" width="90%" cellpadding="0" cellspacing="0" align="center"> 
            <tr bgcolor="#000000">
              <td align="center" class="cabecalho-logo">
                <br>
                <img src="images/Top_Logo.png" alt="ByteBank - digital de verdade">
                <br>
              </td>
            </tr>
        
            <tr>
              <td align="center" >
                <br>
                  <h1>Luisa</h1>
                <br>
              </td>
            </tr>
        
        
            <tr>
              <td align="center">
                <span style="font-size: 25px; font-family: sans-serif">
                  Oi, <strong>${user.name}</strong>
                  <br><br>
                </span>      
              </td>
            </tr>
            <tr>
              <td align="center">
                <span style="font-size: 25px; font-family: sans-serif">
                  <strong>Aproveite a nossa plataforma</strong>
                  <br><br>
                </span>      
              </td>
            </tr>
        
            <tr>
              <td align="center">
                <span style="font-size: 15px; font-family: sans-serif">
                  Temos alguns vídeos que podem te ajudar na sua caminhada de práticas na nossa plataforma
                  <br><br>
                </span>
              </td>
            </tr>
        
            <tr>
              <td align="center">
                <a href="#" class="link-veja sem-underline">
                  Veja o vídeo >
                </a>
        
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        
                <a href="#" class="link-veja sem-underline">
                  Veja a apresentação >
                </a>
              </td>
            </tr>
        
            <!-- Parte verde -->
            <tr  class="parte-verde">
              <td bgcolor="#47a138" align="center">
                <img src="images/Logo.jpg" alt="">
              </td>
            </tr>
        
            <tr bgcolor="#47a138">
              <td align="center">
                <table>
                  <tr>
                    <td width="200" class="vantagem">
                      <table>
                        <tr>
                          <td>
                            <img src="images/Icone_Cadastro_Facilitado.png" class="vantagem-icone">
                            <span class="vantagem-titulo">
                              Cadastro fácil
                            </span>
                            <br>
                            <span class="vantagem-texto">
                              Ingresso simples, sem burocracia, sem análises extensas e com muita rapidez.   
                            </span>
                            <br>
                            <hr style="border-color: #195a0e">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td width="135">
                      &nbsp;
                    </td>
                    <td width="200" class="vantagem">
                      <table>
                        <tr>
                          <td>
                            <img src="images/Icone_Online.png" class="vantagem-icone">
                            <span class="vantagem-titulo">
                              100% online
                            </span>
                            <br>
                            <span class="vantagem-texto">
                              Resolva tudo sem a necessidade de ir ao banco ou perder tempo ao telefone.  
                            </span>
                            <br>
                            <hr style="border-color: #195a0e">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
        
            <!-- Parte preta -->
            <tr bgcolor="#000000">
              <td>
                <table align="center">
                  <tr>
                    <td>
                      <td align="right">
                        <a href="#" class="redeSocial">
                          <img src="images/facebook.png" alt="">
                        </a>
                      </td>
                      <td align="center" width="15%">
                        <a href="#" class="redeSocial">
                          <img src="images/twitter.png" alt="#">
                        </a>
                      </td>
                      <td>
                        <a href="#" class="redeSocial">
                          <img src="images/instagram.png" alt="">
                        </a>
                      </td>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
        
            <tr bgcolor="#000000" align="center">
              <td>
                <span class="rodape-texto">
                  Dúvidas? Fique à vontade para nos contatar no <a href="email@email.com" class="rodape-texto--link">email@email.com</a>
                </span>
                <br><br>
              </td>
            </tr>
        
            
          </table>
        `;

        db.collection('mail').add({
            to: user.email,
            from: '<lucio.leo85@gmail.com>',
            message: {
                subject: "Bém-vindo a Luisa",
                html: `${email}`
            }
        });

    });

export const deleteUnverifiedAccounts = scheduleDeleteUnverifiedAccounts;
  