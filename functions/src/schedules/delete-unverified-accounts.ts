import * as functions from 'firebase-functions';
import { auth } from "../init";
const moment = require('moment');

export const scheduleDeleteUnverifiedAccounts = functions.pubsub.schedule('0 0 * * *')
  .timeZone('America/Bahia')
  .onRun((context) => {
    functions.logger.log('Iniciando rotina para excluir contas com email não verificado por mais de 7 dias');

    const users: any = []
    let unVerifiedUsers: any = [];
    const listAllUsers = async (nextPageToken?: any) => {
      return auth.listUsers(1000, nextPageToken).then((listUsersResult) => {
        listUsersResult.users.forEach((userRecord) => {
          users.push(userRecord)
        });
        if (listUsersResult.pageToken) {
          listAllUsers(listUsersResult.pageToken);
        }
      }).catch((error) => {
        functions.logger.error('Error listing users:', error);
      });
    };

    listAllUsers().then(() => {
      unVerifiedUsers = users
        .filter((user: any) => {
          const creationTime = moment(new Date(user.metadata.creationTime));
          const currentDate = moment(new Date());
          const daysDiff = moment.duration(currentDate.diff(creationTime)).asDays();
          return !user.emailVerified && Number(daysDiff) > 3;
        })
        .map((user: any) => user.uid);
      //DELETando usuários
      return auth.deleteUsers(unVerifiedUsers).then((deleteUsersResult) => {
        functions.logger.log(`deletado com sucesso usuário: ${deleteUsersResult.successCount}`);
        deleteUsersResult.errors.forEach((err) => {
          functions.logger.error(err.error.toJSON());
        });
        return true
      }).catch((error) => {
        functions.logger.error('Erro ao deletar usuário:', error);
        return false
      });
    });
    return null;
  });
