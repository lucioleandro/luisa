import * as functions from 'firebase-functions';
import { auth } from "./init";

export function getUserCredentialsMiddleware(req: any, res: any, next: any) {

    functions.logger.log(`Tentando extrair credenciais do usuário`);
    
    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        
        const jwt = req.headers.authorization.split('Bearer ')[1];
        if (jwt) {
            auth.verifyIdToken(jwt)
                .then(jwtPayload => {
                     req["uid"] = jwtPayload.uid;
                     req["isProfileCompleted"] = jwtPayload.isProfileCompleted;
    
                    functions.logger.log(
                        `Credentials: uid=${jwtPayload.uid}, isProfileCompleted=${jwtPayload.isProfileCompleted}`);
                    next();
                })
                .catch(error => {
                    functions.logger.error("Erro na validação do token", error);
                    res.status(403).send('Acesso negado!');
                    return;
                });
        }
        else {
            functions.logger.error('Acesso negado', 'Requisição não possui token no formato correto');
            res.status(403).send('Acesso negado!');
            return;
        }
    } else {
        functions.logger.error('Acesso negado', 'Requisição não possui token de autenticação');
        res.status(403).send('Acesso negado!');
        return;
    }

}